
-- Here is where all of your serverside functions should go.

-- Example server function that will slap the given player.
function Schema:SlapPlayer(client)
	if (IsValid(client) and client:IsPlayer()) then
		client:SetVelocity(Vector(math.random(-50, 50), math.random(-50, 50), math.random(0, 20)))
		client:TakeDamage(math.random(5, 10))
	end
end


ix.option.Add("resaleValue", ix.type.number, false, {
	category = "Selling",
	min = 0.0, max = 0.9,
	hidden = isHidden,
	OnChanged = function(oldValue, value)
		
	end
})