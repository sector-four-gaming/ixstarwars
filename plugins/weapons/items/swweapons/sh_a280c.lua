ITEM.name = "A-280c"
ITEM.description = "An A-280c"
ITEM.model = "models/sw_battlefront/weapons/a280c_carbine_ext.mdl"
ITEM.class = "tfa_blaster_a280c"
ITEM.weaponCategory = "primary"
ITEM.width = 3
ITEM.height = 2
ITEM.price = 8500
ITEM.flag = "a"
ITEM.isWeapon = true
ITEM.isPLWeapon = true
ITEM.isTFA = true
ITEM.holsterDrawInfo = {
	bone = "ValveBiped.Bip01_Spine2",
	ang = Angle(180, 0,20),
	pos = Vector(4, 10, -2),
}
ITEM.modifier = 10
ITEM.weight = 3.850