PLUGIN.name = "SW Weapons"
PLUGIN.author = "Lt. Taylor"
PLUGIN.description = "Adds TFA SWEPs"
PLUGIN.repairFlag = "R"

ix.flag.Add(PLUGIN.repairFlag, "Access to repair things.")

if (CLIENT) then
	local PANEL = {}
	function PANEL:Init()
	    local DuraSlider = self:Add("DNumSlider")
		DuraSlider:SetText("Weapon Durability")
		DuraSlider:SetMin(0)
		DuraSlider:SetMax(100)
		DuraSlider:SetDecimals(0)
		DuraSlider:Dock(FILL)
		
		self:SetTitle("Set Weapon Durability")
		self:SetSize(400, 150)
		self:Center()
		self:MakePopup()

		self.submit = self:Add("DButton")
		self.submit:Dock(BOTTOM)
		self.submit:DockMargin(0, 5, 0, 0)
		self.submit:SetTall(25)
		self.submit:SetText("Confirm")
		self.submit.DoClick = function()
		    local dura = DuraSlider:GetValue()
    		netstream.Start("wepdurabilityAdjust", (dura * 100), self.itemID)
    		self:Close()
		end
	end

	function PANEL:Think()
		self:MoveToFront()
	end

	vgui.Register("nutWepDurabilityMenu", PANEL, "DFrame")

	netstream.Hook("wepdurabilityAdjust", function(dura, id)
		local adjust = vgui.Create("nutWepDurabilityMenu")

		if (id) then
			adjust.itemID = id
		end
	end)
else
	netstream.Hook("wepdurabilityAdjust", function(client, dura, id)
		local inv = (client:GetChar() and client:GetChar():GetInv() or nil)

		if (inv) then
			local item
			if (id) then
				item = ix.item.instances[id]
    			local ent = item:GetEntity()
    			if (item and (IsValid(ent) or item:GetOwner() == client)) then
    				(ent or client):EmitSound("buttons/combine_button1.wav", 50, 170)
                    dura = math.Round(dura)
    				item:SetData("wepdurability", dura)
    			else
    				client:notify("No Weapon")
    			end
			end
		end
    end)
end

if (CLIENT) then return end

function PLUGIN:SWWeaponFired(entity,itemid)
    if entity:IsPlayer() then
		local wepclass = entity:GetActiveWeapon():GetClass()
		local item = entity:GetChar():GetInv():GetItemByID(itemid)

		if (item:GetData("wepdurability", 10000) ~= 0) then
			item:SetData("wepdurability", item:GetData("wepdurability", 10000) - math.Round(item.modifier/2))
		end
							
		if (item:GetData("wepdurability", 10000) <= 0) then
			item:SetData("equip", false)
			entity:StripWeapon(v.class)
		end
    end
end

hook.Add("SWWeaponFired", "Weapon_Fired", WeaponFired)